from datetime import datetime

from db import Database
from . import bot,chat_id,chat_id_test

db = Database()

def atl():
    data = db.read('select symbol, name, atl_date from atl where atl_date > NOW()::date - 3 order by atl_date desc, name')
    if len(data) > 0:
        text = f"```\n🔴 All Time Low in BINANCE 🔴\n\n"
        for i in data:
            text = text + f"{i[1]:<20} {i[0]:<6} {datetime.strftime(i[2],'%d/%m'):<8}\n"
        text = text + f"```"
        result = bot.sendMessage(chat_id, text, parse_mode = 'Markdown',disable_web_page_preview = True)
        print('Updated ATL')
