from datetime import datetime
import statistics

from db import Database
from . import bot,chat_id,chat_id_test

db = Database()

def update_indicator():
    time = datetime.now().strftime('%Y-%m-%d %H:00:00')
    result = db.update(f"select indicator_binance('{time}');")
    # print(result)
    # if len(result) > 0 :
    #     print(f"Update indicator {time}")

def upsize(time):
    data = ''
    if time == 'DAY':
        data = db.read('''
        SELECT 
            symbol, close, ema_34, ema_89,
            (ema_34 - close)*100/close upsize_34, (ema_89 - close)*100/close upsize_89,
            (close - ema_34)*100/ema_34 downsize_34, (close - ema_89)*100/ema_89 downsize_89
        FROM (
            SELECT j1.time, j1.symbol, j1.close, j2.ema_34, j2.ema_89 
            FROM (
                select time,symbol, close from price_spot 
                where time = (select time from price_spot order by time desc limit 1) 
                and symbol not like '%UPUSDT' and symbol not like '%DOWNUSDT' and symbol not like '%BULLUSDT' and symbol not like '%BEARUSDT' ) j1
            LEFT JOIN
            (select symbol, timeframe, ema_34, ema_89 from (
                select time, symbol, timeframe, close,ema_34, ema_89,
                    ROW_NUMBER () OVER (PARTITION BY symbol ORDER BY time desc) stt
                from indicator_binance where timeframe = 'DAY' and time = (select time from indicator_binance where timeframe = 'DAY' order by time desc limit 1)
            ) t1 where stt = 1) j2
            ON j1.symbol = j2.symbol
        ) a1
        WHERE symbol not in (select * from upsize_ignore)
        ORDER BY upsize_89 desc 
        ''')
    if time == 'H4':
        data = db.read('''
        SELECT 
            symbol, close, ema_34, ema_89,
            (ema_34 - close)*100/close upsize_34, (ema_89 - close)*100/close upsize_89,
            (close - ema_34)*100/ema_34 downsize_34, (close - ema_89)*100/ema_89 downsize_89
        FROM (
            SELECT j1.time, j1.symbol, j1.close, j2.ema_34, j2.ema_89 
            FROM (
                select time,symbol, close from price_spot 
                where time = (select time from price_spot order by time desc limit 1) 
                and symbol not like '%UPUSDT' and symbol not like '%DOWNUSDT' and symbol not like '%BULLUSDT' and symbol not like '%BEARUSDT' ) j1
            LEFT JOIN
            (select symbol, timeframe, ema_34, ema_89 from (
                select time, symbol, timeframe, close,ema_34, ema_89,
                    ROW_NUMBER () OVER (PARTITION BY symbol ORDER BY time desc) stt
                from indicator_binance where timeframe = 'H4' and time = (select time from indicator_binance where timeframe = 'H4' order by time desc limit 1)
            ) t1 where stt = 1) j2
            ON j1.symbol = j2.symbol
        ) a1
        WHERE symbol not in (select * from upsize_ignore)
        ORDER BY upsize_89 desc 
        ''')
    value_89 = [ x[7] for x in data]
    value_34 = [ x[6] for x in data]
    count_up_89 = sum(i >= 0 for i in value_89)
    count_down_89 = sum(i < 0 for i in value_89)
    percent_up_89 = count_up_89*100/(count_up_89+count_down_89)
    percent_down_89 = count_down_89*100/(count_up_89+count_down_89)
    count_up_34 = sum(i >= 0 for i in value_34)
    count_down_34 = sum(i < 0 for i in value_34)
    percent_up_34 = count_up_34*100/(count_up_34+count_down_34)
    percent_down_34 = count_down_34*100/(count_up_34+count_down_34)
    percent_34 = statistics.mean([ x[6] for x in data])
    percent_89 = statistics.mean(value_89)
    uplist = sorted(data, key=lambda x: x[5],reverse = True)
    downlist = sorted(data, key=lambda x: x[7],reverse = True)
    text = f"```\n{count_up_34}💚{round(percent_up_34,2)}%    {round(percent_34,2)}%    {count_down_34}💔{round(percent_down_34,2)}%\n"
    text = text + f"{count_up_89}💚{round(percent_up_89,2)}%    {round(percent_89,2)}%    {count_down_89}💔{round(percent_down_89,2)}% \n_________________________________\n"
    text = text + f"🌈 {time} Upsize in BINANCE 🌈\n\n"
    for i in uplist[0:20]:
        symbol = i[0][:-4]
        close = i[1]
        upsize34 = str(round(i[4],2))+'%'
        upsize89 = str(round(i[5],2))+'%'
        text = text + f"{symbol:<7} {upsize34:<8} {upsize89:<8} {close} \n"
    text = text + f"_________________________________\n{'SYMBOL':<7} {'UP_1':<8} {'UP_2':<8} {'CLOSE'}\n\n"
    text = text + f"\n🌈 {time} Downsize in BINANCE 🌈\n\n"
    for i in downlist[0:20]:
        symbol = i[0][:-4]
        close = i[1]
        downsize34 = str(round(i[4],2))+'%'
        downsize89 = str(round(i[5],2))+'%'
        text = text + f"{symbol:<7} {downsize34:<8} {downsize89:<8} {close} \n"
    text = text + f"_________________________________\n{'SYMBOL':<7} {'DOWN_1':<8} {'DOWN_2':<8} {'CLOSE'}\n\n```"
    # print(text)
    result = bot.sendMessage(chat_id, text, parse_mode = 'Markdown',disable_web_page_preview = True)
    print(f"Noti {time} Upsize")
