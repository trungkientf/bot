import requests
import json


def getChart(symbol):
    url = "https://www.web2pdfconvert.com/api/convert/web/to/png?storefile=true&filename=chart"
    data = {
        'url':f'https://s.tradingview.com/widgetembed/?frameElementId=tradingview_57e12&symbol={symbol}USDT&interval=30&hidesidetoolbar=1&toolbarbg=f1f3f6&theme=light',
        'pricing':'monthly',
        'DstFileFormat':'png',
        'ConversionDelay':'5',
        'CookieConsentBlock':'true',
        'Zoom':'1',
        'ImageWidth':'800',
        'ImageHeight':'600',
        'ParameterPreset':'Custom'
        }
    headers = {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36",
    }
    response = requests.post(url,data=data)

    img = json.loads(response.text)['Files'][0]['Url']
    return(img)