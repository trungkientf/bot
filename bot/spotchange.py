import requests
from bs4 import BeautifulSoup
from . import bot,chat_id

def spot_change(time):
    responseUp = requests.get('https://ckcoin.top/Spot/Up', headers = {'user-agent':'insomnia/2022.5.1'})
    dataUp = BeautifulSoup(responseUp.text, 'html.parser')
    responseDown = requests.get('https://ckcoin.top/Spot/Down', headers = {'user-agent':'insomnia/2022.5.1'})
    dataDown = BeautifulSoup(responseDown.text, 'html.parser')
    text = f"```\n💚 {time} pump in BINANCE 💚\n\n"
    element = 0
    if time == '4H':
        element = 11
    elif time == '1D':
        element = 14
    for index,i in enumerate(dataUp.find_all('div',class_='col-md-4')[element].table):
        if index in [3,5,7,9,11,13,15,17,19,21]:
            ticker = i.contents[1].contents[1].text
            old = i.contents[3].text.strip()
            new = i.contents[5].text.strip()
            percent = i.contents[7].text.strip()
            text = text + f"{ticker:<6} {old:<8} {new:<8} {percent:<7}\n"
    text = text + f"_________________________________\n{'NAME':<6} {'OLD':<8} {'NEW':<8} {'CHANGE':<7}\n\n💔 {time} dump in BINANCE 💔\n\n"
    for index,i in enumerate(dataDown.find_all('div',class_='col-md-4')[element].table):
        if index in [3,5,7,9,11,13,15,17,19,21]:
            ticker = i.contents[1].contents[1].text
            old = i.contents[3].text.strip()
            new = i.contents[5].text.strip()
            percent = i.contents[7].text.strip()
            text = text + f"{ticker:<6} {old:<8} {new:<8} {percent:<7}\n"
    text = text + f"_________________________________\n{'NAME':<6} {'OLD':<8} {'NEW':<8} {'CHANGE':<7}\n```"
    # print(text)
    # url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={text}&parse_mode=Markdown&disable_web_page_preview=true" #&parse_mode=MarkdownV2
    result = bot.sendMessage(chat_id, text, parse_mode = 'Markdown',disable_web_page_preview = True)
    print('Noti 4H change')

def change_size():
    print('ok')
