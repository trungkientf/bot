
from db import Database
from . import bot,chat_id_other

db = Database()

def noti_ggtrend():
    # db.open()
    data = db.read("SELECT * from ggtrends where noti = 0 order by time")
    if len(data) >0 :
        for i in data:
            time = i[1].strftime('%d/%m')
            keyword = (i[3])
            trafic = (i[4][5:])
            article = (i[5])
            url = (i[6])
            text = f"{keyword} <i>[{time} - {trafic}]</i> \n\n{article} <a href='{url}'>Đọc thêm</a>"
            result = bot.sendMessage(chat_id_other,text,parse_mode = 'HTML',disable_web_page_preview = True)

            if result.message_id:
                db.update(f"UPDATE ggtrends SET noti = 1 WHERE id = {i[0]}")
                print(time,keyword)
    # db.close()