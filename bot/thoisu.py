
from db import Database
from . import bot,chat_id_thoisu

db = Database()

def noti_thoisu():
    db.open()
    data = db.read("SELECT * from thoisu where noti = 0 order by time")
    if len(data) >0 :
        for i in data:
            time = i[3].strftime('%H:%M %d/%m')
            title = i[1]
            description = i[4]
            url = i[5]
            category = i[6]
            image = i[7]
            source = i[8]
            if source == 'thanhnien':
                text = f"{title} ... <a href='{url}'>Đọc thêm</a> \n\n 🕢 {time} - <i>[Thanhnien]</i>"
                result = bot.sendMessage(chat_id_thoisu,text,parse_mode = 'HTML',disable_web_page_preview = True)
                if result.message_id:
                    db.update(f"UPDATE thoisu SET noti = 1 WHERE id = {i[0]}")
                    print(time,source)
            if source == 'vnexpress':
                text = f"{title} \n\n{description} ... <a href='{url}'>Đọc thêm</a> \n\n 🕢 {time} - <i>[Vnexpress / {category}]</i>"
                # result = bot.send_photo(chat_id_thoisu,image,text,parse_mode = 'HTML')
                result = bot.sendMessage(chat_id_thoisu,text,parse_mode = 'HTML',disable_web_page_preview = True)
                # print(chat_id_thoisu,image,text)
                if result.message_id:
                    db.update(f"UPDATE thoisu SET noti = 1 WHERE id = {i[0]}")
                    print(time,source)
            if source == 'dantri':
                text = f"{title} \n\n{description} ... <a href='{url}'>Đọc thêm</a> \n\n 🕢 {time} - <i>[Dantri / {category}]</i>"
                result = bot.sendMessage(chat_id_thoisu,text,parse_mode = 'HTML',disable_web_page_preview = True)
                if result.message_id:
                    db.update(f"UPDATE thoisu SET noti = 1 WHERE id = {i[0]}")
                    print(time,source)
            if source == 'tuoitre':
                text = f"{title} ... <a href='{url}'>Đọc thêm</a> \n\n 🕢 {time} - <i>[Tuoitre / {category}]</i>"
                result = bot.sendMessage(chat_id_thoisu,text,parse_mode = 'HTML',disable_web_page_preview = True)
                if result.message_id:
                    db.update(f"UPDATE thoisu SET noti = 1 WHERE id = {i[0]}")
                    print(time,source)
    db.close()