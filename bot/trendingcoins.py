import requests
import json

from db import Database
from . import bot, chat_id,chat_id_test
from bot.chart import getChart

db = Database()

def noti_trendingcoin():
    db.open()
    data = db.read('SELECT * from trendingcoins where noti = 0 order by time')
    if len(data) > 0 :
        for i in data:
            time = i[1].strftime('%H:%M %d/%m')
            symbol = (i[2])
            name = (i[3])
            # score = str(i[5])
            note = (i[6])
            url = (i[7])
            category_raw = db.read(f"SELECT category FROM category WHERE symbol = '{symbol}' ")
            categories = []
            for j in category_raw:
                categories.append(j[0])
            cate = ', '.join(categories)
            category = '\n\n' + cate if len(categories) > 0 else ''
            text = f"🚀 {name} (<a href='{url}'>{symbol}</a>) {category} \n\n{note}_____________\n🕢 {time}"
            # response = requests.get(f'https://api.chart-img.com/v1/tradingview/advanced-chart/storage?symbol={symbol}USDT&interval=30m&theme=light&key=mk4aMflA9h7guP2fWDpIA94aYam5jCAq4lSNLZw1')
            # image = json.loads(response.text).get('url')
            image = getChart(symbol)
            result = bot.send_photo(chat_id,image,caption=text,parse_mode = 'HTML')
            # result = bot.sendMessage(chat_id, text, parse_mode = 'HTML',disable_web_page_preview = True)
            if result.message_id:
                db.update(f"UPDATE trendingcoins SET noti = 1 WHERE id = {i[0]}")
                print(time,symbol)
    db.close()
