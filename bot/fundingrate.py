import requests
import json
from db import Database
from datetime import datetime

from . import bot,chat_id,chat_id_test

db = Database()

def ranking_funding():
    value = db.read("SELECT * FROM funding_rate_limit WHERE id = 1")[0][1]
    response = requests.get('https://www.binance.com/fapi/v1/premiumIndex')
    data = json.loads(response.text)
    trans = [{'symbol':x.get('symbol'),'lastFundingRate':round(float(x.get('lastFundingRate'))*100.0,4)} for x in data]
    result = sorted(trans, key=lambda x: x.get('lastFundingRate'),reverse = False)
    filtered = list(filter(lambda x: (x['lastFundingRate'] > value or x['lastFundingRate'] < -value),result))
    # for i in filtered:
    #     print(datetime.now().strftime('%Y-%m-%d %H:00:00'),i['symbol'],i['lastFundingRate'])
    if len(filtered) > 0:
        text = f'```\n🛎️ Funding rate [{str(value)}]: \n\n'
        for i in filtered:
            text = text + f"{i['symbol']:<15} {i['lastFundingRate']}% \n"
        text = text + '```'
        bot.sendMessage(chat_id,text,parse_mode = 'Markdown')
        # print(text)
        print('Noti Funding rate ranking')

def calculate_funding(n,value):
    if n > value:
        return 1
    elif n < -value: 
        return -1
    else:
        return 0

def update_funding():
    value = db.read("SELECT * FROM funding_rate_limit WHERE id = 1")[0][1]
    response = requests.get('https://www.binance.com/fapi/v1/premiumIndex')
    data = json.loads(response.text)
    trans = [{'symbol':x.get('symbol'),'lastFundingRate':round(float(x.get('lastFundingRate'))*100.0,4)} for x in data]
    sort = sorted(trans, key=lambda x: x.get('lastFundingRate'),reverse = False)
    result = list(map(lambda n: {'symbol':n['symbol'], 'fundingrate': n['lastFundingRate'], 'overfunding': calculate_funding(n['lastFundingRate'],value)},trans))
    filter_over = list(filter(lambda x: (x['overfunding'] in [-1,1]),result))
    # print('list over',filter_over)
    funding_over = db.read('SELECT * FROM funding_rate_binance WHERE overfunding IN (1,-1)')
    # print('list over in db',funding_over)

    # funding rate change from normal to over:
    # Loop mấy thằng đang over
    for i in filter_over:
        symbol = i['symbol']
        # Lấy data trong db của mấy thằng over
        funding_old = db.read(f"SELECT * FROM funding_rate_binance WHERE symbol = '{symbol}'")
        # print('data from db with symbol in loop' ,funding_old)
        # Kiểm tra bằng cách lọc xem trong mấy thằng bị over trong db có thằng over hiện tại k
        check = list(filter(lambda x: x[0] == symbol,funding_over))
        # Nếu không có thì nó vừa over --> Noti nó
        if len(check) == 0 and len(funding_old)>0:
            fundingrate_old = funding_old[0][1]
            fundingrate_now = i['fundingrate']
            text = f"```\n🛎️ Funding rate OVER [{value}]: \n\n{symbol:<10} {fundingrate_old}%  =>  {fundingrate_now}% ```"
            bot.sendMessage(chat_id,text,parse_mode = 'Markdown')
            print('Noti Funding rate ranking')


    # funding rate change from over to normal:
    # Loop mấy thằng đang over trong db
    for i in funding_over:
        symbol = i[0]
        # print(symbol,i[1],i[2])
        # Filter thằng over đó hiện tại
        funding_present = list(filter(lambda x: (x['symbol'] == symbol),result))
        # print(funding_present)
        # Kiểm tra xem đã hết over chưa - xét trường hợp đang over trong db nhưng hiện tại bị delist?
        if len(funding_present) > 0:
            if funding_present[0]['overfunding'] ==0:
                fundingrate_old = i[1]
                fundingrate_now = funding_present[0]['fundingrate']
                text = f"```\n🛎️ Funding rate NORMAL [{value}]: \n\n{symbol:<10} {fundingrate_old}%  =>  {fundingrate_now}% ```"
                bot.sendMessage(chat_id,text,parse_mode = 'Markdown')
                print('Noti Funding rate ranking')


    # Update funding rate
    for i in result:
        symbol = i['symbol']
        fundingrate = i['fundingrate']
        overfunding = i['overfunding']
        db.update(f"INSERT INTO funding_rate_binance (symbol, fundingrate, overfunding) VALUES('{symbol}',{fundingrate},{overfunding})ON CONFLICT (symbol) DO UPDATE SET fundingrate = {fundingrate}, overfunding = {overfunding}")
    print('Updated funding rate')
