import requests
import json

def vc():
    response = requests.get('https://www.binance.com/bapi/composite/v1/public/marketing/symbol/list')
    data = json.loads(response.text)['data']
    print(len(data))
    data_filter = []
    for i in data:
        if i['marketCap'] != 0 and i['marketCap'] != None:
            data_filter.append(i)
    print(len(data_filter))
    vc_raw = [{'symbol': i['symbol'],'vc': i['volume']/i['marketCap'],'name': i['fullName']} for i in data_filter]
    vc = sorted(vc_raw, key=lambda x: x['vc'],reverse = True)
    for i in vc[0:60]:
        print(i)
    