import psycopg2

class Database():
    def __init__(self, db="allin", user="postgres", password="password", host="45.76.154.242", port="5432"):
        self.conn = psycopg2.connect(dbname=db, user=user, password=password, host=host, port=port)
        self.cursor = self.conn.cursor()
    
    def open(self, db="allin", user="postgres", password="password", host="45.76.154.242", port="5432"):
        self.conn = psycopg2.connect(dbname=db, user=user, password=password, host=host, port=port)
        self.cursor = self.conn.cursor()

    def read(self, query):
        self.cursor.execute(query)
        return  self.cursor.fetchall()

    def update(self, query):
        self.cursor.execute(query)
        self.conn.commit()

    def close(self):
        self.cursor.close()
        self.conn.close()
