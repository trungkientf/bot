FROM python:3.10-slim

RUN echo "Asia/Ho_Chi_Minh" > /etc/timezone

RUN rm -f /etc/localtime

RUN dpkg-reconfigure -f noninteractive tzdata

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN python3 -m pip install --upgrade pip

RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python3", "app.py" ]