from apscheduler.schedulers.background import BackgroundScheduler
from time import sleep
from db import Database

from bot.test import test
from bot.trendingcoins import noti_trendingcoin
from bot.spotchange import spot_change
from bot.ggtrend import noti_ggtrend
from bot.twitter import save_tweets,noti_tweet
from bot.fundingrate import ranking_funding, update_funding
from bot.thoisu import noti_thoisu
from bot.atl import atl
from bot.upsize import update_indicator, upsize

db = Database()
# test()
# db.open()
# noti_trendingcoin()

def runTask():
    # db.open()
    noti_thoisu()
    # noti_trendingcoin()


scheduler = BackgroundScheduler()
# scheduler.add_job(lambda: save_tweets(), 'interval', minutes= 2)
# scheduler.add_job(lambda: noti_tweet(), 'interval', minutes= 2)
# scheduler.add_job(lambda: noti_trendingcoin(), 'interval', minutes= 2)
# scheduler.add_job(lambda: noti_ggtrend(), 'interval', minutes= 2)
# scheduler.add_job(lambda: noti_thoisu(), 'interval', minutes= 2)
scheduler.add_job(lambda: runTask(), 'interval', minutes= 2)
# scheduler.add_job(lambda: update_funding(), 'interval', minutes= 2)
scheduler.add_job(lambda: spot_change('4H'), 'cron', hour= '3,7,11,15,19,23')
scheduler.add_job(lambda: spot_change('1D'), 'cron', hour= 7)
# scheduler.add_job(lambda: ranking_funding(), 'cron', minute= 1)

# scheduler.add_job(lambda: update_indicator(), 'cron', minute= 2)
# scheduler.add_job(lambda: upsize('DAY'), 'cron', hour= '7', minute=6)
# scheduler.add_job(lambda: upsize('H4'), 'cron', hour= '3,7,11,15,19,23', minute=5)

# scheduler.add_job(lambda: atl(), 'cron', hour= 6, minute= 30)
# scheduler.add_job(lambda: noti_trendingcoin(), 'cron', minute= '*/5')

scheduler.start()

while True:
    sleep(1)